# Talent katapult 2018 - GraphQL ASP.NET Core

Dette repo indeholder en kodebase for graphQL i ASP.NET Core.
Der er lavet en docker-compose fil og en docker file til at deploy kode basen i en docker container. 
Der vil blive deployet to containers, en med API'et kørende med GraphQL og et grafisk interface til at lave queries med.
Den anden container indeholder en MSSQL database som API'et snakker op i mod.

## Up and running

For at få docker til at køre kræves docker på maskinen med docker-compose.
Containerne kan deployes ved følgende komandoer i roden af repo'et.

```bash
> docker-compose build
> docker-compose up
```

API'et er startet op når følgende besked står på consolen, API'et kører **dotnet watch**, dvs. ændring i `.cs` filer lokalt vil automatisk blive re-kompilet og API'et vil starte op igen.

```bash
backend    | Hosting environment: Development
backend    | Content root path: /code/app
backend    | Now listening on: http://[::]:5000
backend    | Application started. Press Ctrl+C to shut down.
```

Når containerne er oppe at kører, kan man på lokal-maskinen åben det grafisk interface til at lave queries i på følgende url
http://localhost:5000/graphiql

Følgende query kan bruges til at test om det virker:

```javascript
query {
  book(isbn: "1234"){
    name
  }
}
```

Resultat:

```javascript
{
  "data": {
    "book": {
      "name": "Hest"
    }
  }
}
```

### Connect til databasen lokalt

Connect til databasen der kører i docker containeren fra lokal maskine kan gøres således:

```
servername: localhost, 1433
sql server auth:
    login: sa
    password: ITMinds2018
```

## Opgaver

1. Implementer query for graph typen `Author`.
2. Implementer query for graph typen `Publisher`.
3. Udvide graph typerne med typen `Store`, som har felterne `Name`, `Addresse`, `List<Books>`, hertil skal der laves en entity framework migration og seed data til storen.
4. Implementer queryen for den nye `Store` type.
5. Implementer mutation for graph typen `Book`.
6. Definere en scalar type `MyDate` ved at nedarve fra klassen `ScalarGraphType` fremfor `ObjectGraphType` og se om denne kan implementeres en af de nuværende typer.
7. Implementer pagination på query `books`

Hjælp kan måske findes her

https://github.com/JuergenGutsch/graphql-aspnetcore

https://graphql-dotnet.github.io/getting-started/#query-validation

GraphQL for .NET https://github.com/graphql-dotnet/graphql-dotnet

Eksempler https://github.com/graphql-dotnet/graphql-dotnet/tree/master/src/GraphQL.StarWars

God fornøjelse!
