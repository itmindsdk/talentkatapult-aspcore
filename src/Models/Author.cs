﻿using System;
using System.Collections.Generic;

namespace aspnetapp.Models
{
    public class Author
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Birthdate { get; set; }

        public virtual ICollection<Book> Books { get; set; } = new HashSet<Book>();
    }
}
