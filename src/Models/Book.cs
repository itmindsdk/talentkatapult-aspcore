﻿namespace aspnetapp.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Isbn { get; set; }

        public string Name { get; set; }

        public int AuthorId { get; set; }
        public virtual Author Author { get; set; }

        public int PublisherId { get; set; }
        public virtual Publisher Publisher { get; set; }
    }
}
