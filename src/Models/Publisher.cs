﻿using System.Collections.Generic;

namespace aspnetapp.Models
{
    public class Publisher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; } = new HashSet<Book>();

        public virtual ICollection<Author> Authors { get; set; } = new HashSet<Author>();
    }
}
