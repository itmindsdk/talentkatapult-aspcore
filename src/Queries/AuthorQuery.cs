﻿using System.Linq;
using aspnetapp.Data;
using aspnetapp.GraphQLTypes;
using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.Queries
{
    public class AuthorQuery : ObjectGraphType, IQuery
    {
        public AuthorQuery(ApplicationDbContext dbContext)
        {
            Field<AuthorType>("author",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType>
                    {
                        Name = "id"
                    }),
                resolve: context =>
                {
                    var id = context.GetArgument<int>("id");
                    return dbContext.Authors.SingleOrDefault(x => x.Id == id);
                });

            Field<ListGraphType<AuthorType>>("authors",
                resolve: context => dbContext.Authors);
        }
    }
}
