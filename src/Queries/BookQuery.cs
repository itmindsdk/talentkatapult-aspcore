﻿using System.Linq;
using aspnetapp.Data;
using aspnetapp.GraphQLTypes;
using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.Queries
{
    public class BookQuery : ObjectGraphType, IQuery
    {
        public BookQuery(ApplicationDbContext dbContext)
        {
            Field<BookType>("book",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType>
                    {
                        Name = "isbn"
                    }),
                resolve: context =>
                {
                    var id = context.GetArgument<string>("isbn");
                    return dbContext.Books.SingleOrDefault(x => x.Isbn == id);
                });

            Field<ListGraphType<BookType>>("books",
                resolve: context => dbContext.Books);
        }
    }
}