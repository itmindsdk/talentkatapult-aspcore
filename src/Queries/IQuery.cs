using System.Collections.Generic;
using GraphQL.Types;

namespace aspnetapp.Queries
{
    public interface IQuery
    {
        IEnumerable<FieldType> Fields { get; }
    }
}
