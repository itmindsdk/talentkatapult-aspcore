﻿using System.Linq;
using aspnetapp.Data;
using aspnetapp.GraphQLTypes;
using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.Queries
{
    public class PublisherQuery : ObjectGraphType, IQuery
    {
        public PublisherQuery(ApplicationDbContext dbContext)
        {
            Field<PublisherType>("publisher",
                arguments: new QueryArguments(
                    new QueryArgument<IntGraphType>
                    {
                        Name = "id"
                    }),
                resolve: context =>
                {
                    var id = context.GetArgument<int>("id");
                    return dbContext.Publishers.SingleOrDefault(x => x.Id == id);
                });

            Field<ListGraphType<PublisherType>>("publishers",
                resolve: context => dbContext.Publishers);
        }
    }
}
