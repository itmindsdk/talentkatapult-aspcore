using System;
using System.Collections.Generic;
using System.Linq;
using aspnetapp.Queries;
using GraphQL.Types;

namespace aspnetapp.Queries
{
    public class RootQuery : ObjectGraphType
    {
        public RootQuery(IEnumerable<IQuery> queries)
        {
            queries.ToList().ForEach(q => q.Fields.ToList().ForEach(f => AddField(f)));
        }
    }
}
