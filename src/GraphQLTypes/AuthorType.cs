﻿using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.GraphQLTypes
{
    public class AuthorType : ObjectGraphType<Author>
    {
        public AuthorType()
        {
            Field(x => x.Id).Description("The Id of the person.");
            Field(x => x.Name).Description("The name of the person.");
            Field(x => x.Birthdate).Description("The birthdate of the person.");
            Field<ListGraphType<BookType>>("books", "The books by the author.");
            Field<ListGraphType<PublisherType>>("publisher", "The publishers the author is assigned to.");
        }
    }
}
