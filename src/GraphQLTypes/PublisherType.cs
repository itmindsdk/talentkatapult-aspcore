﻿using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.GraphQLTypes
{
    public class PublisherType : ObjectGraphType<Publisher>
    {
        public PublisherType()
        {
            Field(x => x.Id).Description("The id of the publisher.");
            Field(x => x.Name).Description("The name of the publisher.");
            Field<ListGraphType<BookType>>("books", "The books published by the publisher.");
            Field<ListGraphType<AuthorType>>("authors", "The authors asigned to the publisher.");
        }
    }
}
