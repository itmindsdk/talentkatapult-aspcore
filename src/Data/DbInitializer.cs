﻿using System;
using System.Linq;
using aspnetapp.Models;
using Microsoft.EntityFrameworkCore;

namespace aspnetapp.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Books.Any())
                return; // DB has been seeded

            var books = new[]
            {
                new Book
                {
                    Isbn = "1234",
                    Name = "Hest",
                    Author = new Author {Birthdate = DateTime.Now, Name = "Foo"},
                    Publisher = new Publisher {Name = "Evil corp."}
                }
            };

            context.Books.AddRange(books);

            context.SaveChanges();
        }
    }
}
