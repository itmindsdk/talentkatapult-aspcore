﻿using System.Linq;
using System.Reflection;
using aspnetapp.Data;
using aspnetapp.Mutations;
using aspnetapp.Queries;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace aspnetapp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            // Configure the default schema
            services.AddGraphQl(schema =>
            {
                schema.SetQueryType<RootQuery>();
                schema.SetMutationType<RootMutation>();
            });

            // Also register all graph types
            var types = Assembly.GetAssembly(typeof(Startup)).GetTypes()
                .Where(x => typeof(IGraphType).IsAssignableFrom(x));

            types.Where(x => !typeof(IQuery).IsAssignableFrom(x) || !typeof(IMutation).IsAssignableFrom(x))
                .ToList().ForEach(x => services.AddScoped(x));

            types.Where(x => typeof(IQuery).IsAssignableFrom(x)).ToList().ForEach(x => services.AddTransient(typeof(IQuery), x));
            types.Where(x => typeof(IMutation).IsAssignableFrom(x)).ToList().ForEach(x => services.AddTransient(typeof(IMutation), x));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseGraphiql("/graphiql", options => { options.GraphQlEndpoint = "/graphql"; });
            }

            app.UseGraphQl("/graphql", options =>
            {
                options.ExposeExceptions = true;
                //options.SchemaName = "OtherSchema"; // only if additional schemas were registered in ConfigureServices
                //options.AuthorizationPolicy = "Authenticated"; // optional
            });

            app.UseMvc();
        }
    }
}