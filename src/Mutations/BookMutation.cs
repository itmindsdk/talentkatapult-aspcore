using System.Linq;
using aspnetapp.Data;
using aspnetapp.GraphQLTypes;
using aspnetapp.Models;
using GraphQL.Types;

namespace aspnetapp.Mutations
{
    public class BookMutation : ObjectGraphType, IMutation
    {
        public BookMutation(ApplicationDbContext dbContext)
        {
            Field<BookType>("createBook",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType>
                    {
                        Name = "isbn"
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "name"
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "authorId"
                    },
                    new QueryArgument<IntGraphType>
                    {
                        Name = "publisherId"
                    }),
                resolve: context =>
                {
                    var book = new Book {
                        Isbn = context.GetArgument<string>("isbn"),
                        Name = context.GetArgument<string>("name"),
                        AuthorId = context.GetArgument<int>("authorId"),
                        PublisherId = context.GetArgument<int>("publisherId")              
                    };

                    dbContext.Add(book);
                    dbContext.SaveChanges();

                    return book;
                });
        }
    }
}
