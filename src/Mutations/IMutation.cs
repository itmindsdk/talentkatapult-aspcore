using System.Collections.Generic;
using GraphQL.Types;

namespace aspnetapp.Mutations
{
    public interface IMutation
    {
        IEnumerable<FieldType> Fields { get; }
    }
}
