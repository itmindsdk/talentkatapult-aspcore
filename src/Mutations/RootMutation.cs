using System;
using System.Collections.Generic;
using System.Linq;
using aspnetapp.Queries;
using GraphQL.Types;

namespace aspnetapp.Mutations
{
    public class RootMutation : ObjectGraphType
    {
        public RootMutation(IEnumerable<IMutation> mutations)
        {
            mutations.ToList().ForEach(m => m.Fields.ToList().ForEach(f => AddField(f)));
        }
    }
}
