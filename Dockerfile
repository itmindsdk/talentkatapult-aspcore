FROM microsoft/aspnetcore-build:2.1.300-preview1 AS build

# install yarn with node
RUN apt-get update \
  && apt-get install -y apt-transport-https \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update \
  && apt-get install -y yarn

# Required inside Docker, otherwise file-change events may not trigger
ENV DOTNET_USE_POLLING_FILE_WATCHER 1

WORKDIR /code/app
EXPOSE 5000

# copy csproj and restore as distinct layers
COPY ./src/*.csproj .
COPY ./src/NuGet.config .
COPY ./src/Directory.Build.props .

RUN dotnet restore

ENTRYPOINT dotnet restore; dotnet watch run